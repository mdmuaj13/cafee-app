package com.example.lenovo.projectalpha;

import android.arch.persistence.room.Dao;
import android.arch.persistence.room.Delete;
import android.arch.persistence.room.Insert;
import android.arch.persistence.room.Query;

import java.util.List;

@Dao
public interface UserDao {

    @Query("SELECT * FROM user")
    List<User> getAll();

    @Query("SELECT * FROM user WHERE email = :email AND password = :password")
    User loginFunction(String email, String password);

    @Query("SELECT COUNT(*) FROM user")
    int totalUser();

    @Insert
    void insertAll(User... users);

    @Delete
    void delete(User user);

}
