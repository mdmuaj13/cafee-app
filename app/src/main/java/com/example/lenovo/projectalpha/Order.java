package com.example.lenovo.projectalpha;

import android.arch.persistence.room.ColumnInfo;
import android.arch.persistence.room.Entity;
import android.arch.persistence.room.PrimaryKey;
import android.support.annotation.NonNull;

import java.sql.Timestamp;
import java.util.Date;

@Entity(tableName = "order")
public class Order {

    @PrimaryKey
    @NonNull
    private String order_time;

    private int uid;

    private String orderItemName;

    private int pid;

    private int orderItemRate;

    private int orderItemQuantity;

    public String getOrder_time() {
        return order_time;
    }

    public void setOrder_time(String order_time) {
        this.order_time = order_time;
    }

    public int getUid() {
        return uid;
    }

    public void setUid(int uid) {
        this.uid = uid;
    }

    public String getOrderItemName() {
        return orderItemName;
    }

    public void setOrderItemName(String orderItemName) {
        this.orderItemName = orderItemName;
    }

    public int getPid() {
        return pid;
    }

    public void setPid(int pid) {
        this.pid = pid;
    }

    public int getOrderItemRate() {
        return orderItemRate;
    }

    public void setOrderItemRate(int orderItemRate) {
        this.orderItemRate = orderItemRate;
    }

    public int getOrderItemQuantity() {
        return orderItemQuantity;
    }

    public void setOrderItemQuantity(int orderItemQuantity) {
        this.orderItemQuantity = orderItemQuantity;
    }
}
