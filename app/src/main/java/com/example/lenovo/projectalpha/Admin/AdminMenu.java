package com.example.lenovo.projectalpha.Admin;

import android.content.Intent;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.view.View;
import android.widget.LinearLayout;
import android.widget.Toast;

import com.example.lenovo.projectalpha.R;

public class AdminMenu extends AppCompatActivity implements View.OnClickListener {

    private LinearLayout lMenu1, lMenu2, lMenu3, lMenu4;


    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.admin_menu);

        lMenu1 = findViewById(R.id.ll_menu1);
        lMenu2 = findViewById(R.id.ll_menu2);
        lMenu3 = findViewById(R.id.ll_menu3);
        lMenu4 = findViewById(R.id.ll_menu4);

        lMenu1.setOnClickListener(this);
        lMenu2.setOnClickListener(this);
        lMenu3.setOnClickListener(this);
        lMenu4.setOnClickListener(this);
    }

    @Override
    public void onClick(View view) {
        if (view.getId() == R.id.ll_menu1) {
            Intent intent = new Intent(AdminMenu.this,Insert.class);
            startActivity(intent);
            finish();

        } else if (view.getId() == R.id.ll_menu2) {
            Intent intent = new Intent(AdminMenu.this,UpdateProduct.class);
            startActivity(intent);
            finish();

        } else if (view.getId() == R.id.ll_menu3) {
            Toast.makeText(this,"Credit MD. Mohaiminul Hasan",Toast.LENGTH_LONG).show();


        } else if (view.getId() == R.id.ll_menu4) {
            Intent intent = new Intent(AdminMenu.this,DeleteProduct.class);
            startActivity(intent);
            finish();

        }
    }

}
