package com.example.lenovo.projectalpha;

import android.content.Intent;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.view.View;
import android.widget.LinearLayout;

import com.example.lenovo.projectalpha.Admin.Admin;
import com.example.lenovo.projectalpha.Admin.AdminMenu;
import com.example.lenovo.projectalpha.Admin.Insert;
import com.example.lenovo.projectalpha.Orders.OrderList;

public class Contents extends AppCompatActivity implements View.OnClickListener{

    private LinearLayout lMenu1,lMenu2,lMenu3,lMenu4;


    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.contents);

        lMenu1 = findViewById(R.id.ll_menu1);
        lMenu2 = findViewById(R.id.ll_menu2);
        lMenu3 = findViewById(R.id.ll_menu3);
        lMenu4 = findViewById(R.id.ll_menu4);

        lMenu1.setOnClickListener(this);
        lMenu2.setOnClickListener(this);
        lMenu3.setOnClickListener(this);
        lMenu4.setOnClickListener(this);

    }

    @Override
    public void onClick(View view) {
        if(view.getId() == R.id.ll_menu1){
            Intent intent = new Intent(Contents.this,ContentList.class);
            startActivity(intent);

        }
        else if(view.getId() == R.id.ll_menu2){
            Intent intent = new Intent(Contents.this,OrderList.class);
            startActivity(intent);
        }
        else if(view.getId() == R.id.ll_menu3){
            Intent intent = new Intent(Contents.this,Credit.class);
            startActivity(intent);
        }
        else if(view.getId() == R.id.ll_menu4){
            Intent intent = new Intent(Contents.this,AdminMenu.class);
            startActivity(intent);
        }
    }
}
