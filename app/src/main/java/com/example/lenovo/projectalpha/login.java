package com.example.lenovo.projectalpha;

import android.content.Intent;
import android.content.SharedPreferences;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.util.Log;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;
import android.widget.Toast;

import java.util.List;

import static java.lang.Integer.valueOf;

public class login extends AppCompatActivity implements View.OnClickListener {
    private EditText etMail, etPass;
    Button btnLogin, btnReg;

    private String sMail, sPass;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_login);

        etMail = findViewById(R.id.et_user);
        etPass = findViewById(R.id.et_pass);


        btnLogin = findViewById(R.id.btn_submit);
        btnReg = findViewById(R.id.btn_reg);

        btnLogin.setOnClickListener(this);
        btnReg.setOnClickListener(this);


    }

    public boolean login() {


        sMail = etMail.getText().toString();
        sPass = etPass.getText().toString();


        AppDatabase db = AppDatabase.getAppDatabase(this);
//        int count = db.userDao().totalUser();
//        Log.d("Row count", String.valueOf(count));
//
//        Toast.makeText(this, String.valueOf(count),Toast.LENGTH_SHORT).show();
//

        Validation valid = new Validation();

        if(valid.isValidEmail(sMail)){
            if(valid.isValidPassword(sPass)){
                User signUser =  db.userDao().loginFunction(sMail, sPass);
                String temp = signUser.getLname().toString();

                SharedPreferences.Editor editor = getSharedPreferences("login",MODE_PRIVATE).edit();
                editor.putInt("uid", signUser.getUid());
                editor.putString("user",signUser.getLname());
                editor.commit();
                Toast.makeText(this, "Log succefully "+temp,Toast.LENGTH_SHORT).show();
                return true;

//                SharedPreferences.Editor editor = (SharedPreferences.Editor) getSharedPreferences("log",MODE_PRIVATE);
//                editor.putInt("ID",signUser.getUid());
//                editor.putString("fname",signUser.getFname().toString());
//                editor.putString("lname",signUser.getLname().toString());
//                editor.putString("email",signUser.getEmail().toString());
//
//                editor.commit();
            }
        }
        return false;

    }


    @Override
    public void onClick(View v) {

        if(v.getId() == R.id.btn_reg){
//            dbinit.populateAsync(AppDatabase.getAppDatabase(this));
//            Log.d("Database","Data Init!");
            Intent intent = new Intent(login.this,Registration.class);
            startActivity(intent);
        }
        else if(v.getId() == R.id.btn_submit){
            if(login()) {
                Intent intent = new Intent(login.this, ContentList.class);
                startActivity(intent);
                finish();
            }
        }

    }

}