package com.example.lenovo.projectalpha;

import android.app.Activity;
import android.content.Context;
import android.content.res.Resources;
import android.graphics.drawable.Drawable;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.BaseAdapter;
import android.widget.ImageView;
import android.widget.TextView;

import java.util.ArrayList;

public class listAdapter extends BaseAdapter{
    ArrayList<String> title;
    ArrayList<String> subTitle;
    ArrayList<String> images;
    Context context;

    private static LayoutInflater inflater = null;
//,ArrayList<String> getImages
    public listAdapter(ContentList mainActivity, ArrayList<String> getTitle,ArrayList<String> getImages ) {
        title = getTitle;
        images = getImages;
        context = mainActivity;

        inflater = (LayoutInflater) context.getSystemService(context.LAYOUT_INFLATER_SERVICE);
    }

    @Override
    public int getCount() {
        return title.size();
    }

    @Override
    public Object getItem(int position) {
        return position;
    }

    @Override
    public long getItemId(int position) {
        return position;
    }

    public class Holder {
        TextView tv_title;
        ImageView imgv;
    }

    @Override
    public View getView(int position, View convertView, ViewGroup parent) {
        Holder holder = new Holder();

        View rootView = inflater.inflate(R.layout.list_item,null);

        holder.tv_title = (TextView)rootView.findViewById(R.id.tvItemName);
        holder.imgv = (ImageView) rootView.findViewById(R.id.iv_title);

        holder.tv_title.setText(title.get(position));
        int id = context.getResources().getIdentifier("drawable/"+images.get(position),null,context.getPackageName());

////+images.get(position)
//        holder.imgv.setImageResource(id);
//        holder.imgv.setImageResource(R.drawable.img0);
//        holder.imgv.setImageDrawable(context.getResources().getDrawable(R.drawable.img2));

        holder.imgv.setImageResource(id);


        return rootView;
    }
}
