package com.example.lenovo.projectalpha;


import android.arch.persistence.room.Dao;
import android.arch.persistence.room.Delete;
import android.arch.persistence.room.Insert;
import android.arch.persistence.room.Query;

import java.util.List;

@Dao
public interface OrderDao {

    @Query("SELECT * FROM `order` WHERE order_time = :orderTime")
    List<Order> getOrderByTime(String orderTime);

    @Query("SELECT * FROM `order` WHERE uid = :uid")
    List<Order> getOrderById(int uid);

    @Query("DELETE FROM `order` WHERE order_time = :orderTime")
    void deleteOrderByTime(String orderTime);

    @Query("DELETE FROM `order` WHERE uid = :uid")
    void delteOrderById(int uid);
    

    @Insert
    void insertOrder(Order... orders);

    @Delete
    void deleteOrder(Order... orders);

}
