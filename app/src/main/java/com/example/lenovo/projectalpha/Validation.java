package com.example.lenovo.projectalpha;

import java.util.regex.Matcher;
import java.util.regex.Pattern;

public class Validation {

    // validating email id
    public boolean isValidEmail(String email) {
        String EMAIL_PATTERN = "^[_A-Za-z0-9-\\+]+(\\.[_A-Za-z0-9-]+)*@"
                + "[A-Za-z0-9-]+(\\.[A-Za-z0-9]+)*(\\.[A-Za-z]{2,})$";

        Pattern pattern = Pattern.compile(EMAIL_PATTERN);
        Matcher matcher = pattern.matcher(email);
        return matcher.matches();
    }

    // validating password with retype password
    public boolean isValidPassword(String pass) {
        if (pass != null && pass.length() > 3) {
            return true;
        }
        return false;
    }
    public boolean isValidID(String pass) {
        if (pass != null && pass.length() > 2) {
            return true;
        }
        return false;
    }


    // validating name
    private boolean isValidName(String name) {
        if (name != null && name.length() > 2) {
            return true;
        }
        return false;
    }
}
