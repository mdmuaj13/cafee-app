package com.example.lenovo.projectalpha;

import android.content.Intent;
import android.content.SharedPreferences;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.util.Log;

import java.text.SimpleDateFormat;
import java.util.Date;
import java.util.List;

public class MainActivity extends AppCompatActivity {

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.splacescreen);

        AppDatabase dbs = AppDatabase.getAppDatabase(this);
        List<Product> p = dbs.productDao().getAllProduct();
        if(p.size() == 0) {
            dbinit.populateAsync(AppDatabase.getAppDatabase(this));
            Log.d("db", "Menu list populate");
            Log.d("db", " populate"+p.size());
        }
        SimpleDateFormat sdfDate = new SimpleDateFormat("yyyy-MM-dd HH:mm:ss");//dd/MM/yyyy
        Date now = new Date();
        String s = sdfDate.format(now);

        Log.d("sp",s);
        SharedPreferences.Editor ed = (SharedPreferences.Editor) getSharedPreferences("login",MODE_PRIVATE).edit();
        ed.putString("orderTime",s.toString());
        ed.commit();

        SharedPreferences poo = getSharedPreferences("login", MODE_PRIVATE);
        String x = poo.getString("orderTime","");
        Log.d("sp",x);




        Thread splaceScreen  = new Thread() {       // init new Thread()
            public void run() {     //run function
                try{
                    sleep( 2 *1000);

                    Intent intent = new Intent(MainActivity.this,Contents.class);
                    startActivity(intent);
                    finish();
                }
                catch (Exception e){

                }
            }
        };  //thread block
        splaceScreen.start();


    }

    @Override
    protected void onDestroy() {
        super.onDestroy();
    }
}
