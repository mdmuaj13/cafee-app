package com.example.lenovo.projectalpha;

import android.arch.persistence.room.Dao;
import android.arch.persistence.room.Delete;
import android.arch.persistence.room.Insert;
import android.arch.persistence.room.Query;

import java.util.List;

@Dao
public interface ProductDao {

    @Query("SELECT * FROM product")
    List<Product> getAllProduct();

    @Query("SELECT * FROM product WHERE pid = :pId")
    List<Product> getProduct(int pId);

    @Query("SELECT * FROM product WHERE pid = :pId")
    Product getProductById(int pId);

    @Query("SELECT * FROM product WHERE pname LIKE :pName")
    Product findByName(String pName);

    @Query("SELECT pid FROM product WHERE pname = :pName")
    int productId(String pName);

    @Query("SELECT quantity FROM product WHERE pname = :pName")
    int productQuantity(String pName);

    @Query("SELECT price FROM product WHERE pname = :pName")
    int productPrice(String pName);

    @Query("UPDATE product SET Quantity = :quantity WHERE pid = :pId")
     void updateQuantity(int quantity, int pId);

    @Query("UPDATE product SET price = :newPrice WHERE pid = :pId")
    void updatePrice(int newPrice, int pId);

    @Query("UPDATE product SET pname = :newName WHERE pid = :pid")
    void updatePname(String newName, int pid);

    @Query("UPDATE product SET pimage = :newImage WHERE pid = :pid")
     void updatePimage(String newImage, int pid);

    @Query("DELETE FROM product WHERE pid = :pid")
    void deleteProduct(int pid);


    @Insert
    void insertAll(Product... products);

    @Delete
    void deleteAll(Product... product);


}
