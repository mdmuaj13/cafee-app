package com.example.lenovo.projectalpha.Admin;

import android.content.Intent;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;

import com.example.lenovo.projectalpha.R;

public class Admin extends AppCompatActivity implements View.OnClickListener{

    private EditText etUser,etPass;
    private Button btnLog;
    String User,Pass;
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.admin_log);

        etUser=findViewById(R.id.et_user);
        etPass=findViewById(R.id.et_pass);

        btnLog =findViewById(R.id.btn_admin);
        btnLog.setOnClickListener(this);
    }

    @Override
    public void onClick(View view) {
        if(view.getId() == R.id.btn_admin){
            User = etUser.getText().toString();
            Pass = etPass.getText().toString();

            if(User == "admin" && Pass == "admin"){
                Intent intent = new Intent(Admin.this, AdminMenu.class);
                startActivity(intent);
                finish();
            }

        }
    }
}
