package com.example.lenovo.projectalpha.Admin;

import android.content.Intent;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;

import com.example.lenovo.projectalpha.AppDatabase;
import com.example.lenovo.projectalpha.R;

public class UpdateProduct extends AppCompatActivity implements View.OnClickListener{

    private EditText etNewName,etPname,etPrice,etQuantity;
    private int mPrice,mQuantity;
    private String mPname,mNewName;
    private Button btnName,btnPrice,btnQuantity;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.update_product);


        etPname = findViewById(R.id.et_update_name);
        etNewName = findViewById(R.id.et_update_name_new);
        etPrice =findViewById(R.id.et_update_price);
        etQuantity = findViewById(R.id.et_update_quantity);

        btnName = findViewById(R.id.btn_update_name);
        btnPrice =findViewById(R.id.btn_update_price);
        btnQuantity = findViewById(R.id.btn_update_quantity);

        btnName.setOnClickListener(this);
        btnPrice.setOnClickListener(this);
        btnQuantity.setOnClickListener(this);

    }

    @Override
    public void onClick(View view) {

        if(view.getId() == R.id.btn_update_name){
            mPname = etPname.getText().toString();
            mNewName = etNewName.getText().toString();
            AppDatabase dbs = AppDatabase.getAppDatabase(this);
            int pid = dbs.productDao().productId(mPname);
            dbs.productDao().updatePname(mNewName,pid);

        }
        else  if(view.getId() == R.id.btn_update_price) {
            mPname = etPname.getText().toString();
            mPrice = Integer.parseInt(etPrice.getText().toString());

            AppDatabase dbs = AppDatabase.getAppDatabase(this);
            int pid = dbs.productDao().productId(mPname);
            dbs.productDao().updatePrice(mPrice,pid);

        }
        else if(view.getId() == R.id.btn_update_quantity){
            mPname = etPname.getText().toString();
            mQuantity = Integer.parseInt(etQuantity.getText().toString());

            AppDatabase dbs = AppDatabase.getAppDatabase(this);
            int pid = dbs.productDao().productId(mPname);
            dbs.productDao().updatePrice(mQuantity,pid);
        }

        Intent intent = new Intent(UpdateProduct.this,AdminMenu.class);
        startActivity(intent);
        finish();

        }
}
