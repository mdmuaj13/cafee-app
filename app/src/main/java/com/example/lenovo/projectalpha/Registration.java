package com.example.lenovo.projectalpha;

import android.content.Intent;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.util.Log;
import android.view.View;
import android.view.View.OnClickListener;
import android.widget.Button;
import android.widget.EditText;
import android.widget.Toast;

import java.util.List;
import java.util.regex.Matcher;
import java.util.regex.Pattern;

public class Registration extends AppCompatActivity implements OnClickListener {

    private EditText etFname, etLname, etEmail, etPassword;
    private Button btnSignup;

    public String sFname, sLname, sEmail, sPassword;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.registration_form);

        etFname = findViewById(R.id.et_fname);
        etLname = findViewById(R.id.et_lname);
        etEmail = findViewById(R.id.et_email);
        etPassword = findViewById(R.id.et_pass);



        btnSignup = findViewById(R.id.btn_sign);
        btnSignup.setOnClickListener(this);

    }




    public void registration() {

        sFname = etFname.getText().toString();
        sLname = etLname.getText().toString();
        sEmail = etEmail.getText().toString();
        sPassword = etPassword.getText().toString();

        if (isValidName(sFname)) {
            if (isValidName(sLname)){
                if (isValidEmail(sEmail)) {
                    if (isValidPassword(sPassword)) {
                        Toast.makeText(this, "Name: " + sFname + " " + sLname + " Email: " + sEmail, Toast.LENGTH_LONG).show();
                        Log.d("reg", "passing");
                        User regUser = new User();

                        regUser.setFname(sFname);
                        regUser.setLname(sLname);
                        regUser.setEmail(sEmail);
                        regUser.setPassword(sPassword);

                        AppDatabase db = AppDatabase.getAppDatabase(this);
                        db.userDao().insertAll(regUser);


                        List<User> userList = db.userDao().getAll();
                        for(int k=0; k<userList.size(); k++){
                            Log.d("data: ", String.valueOf(userList.get(k).getEmail()));
                        }

                    } else {
                        Log.d("reg", "not pass");
                    }
                } else {
                    Log.d("reg", "email");
                }
        }
    }
}

    // validating email id
    private boolean isValidEmail(String email) {
        String EMAIL_PATTERN = "^[_A-Za-z0-9-\\+]+(\\.[_A-Za-z0-9-]+)*@"
                + "[A-Za-z0-9-]+(\\.[A-Za-z0-9]+)*(\\.[A-Za-z]{2,})$";

        Pattern pattern = Pattern.compile(EMAIL_PATTERN);
        Matcher matcher = pattern.matcher(email);
        return matcher.matches();
    }

    // validating password with retype password
    private boolean isValidPassword(String pass) {
        if (pass != null && pass.length() > 3) {
            return true;
        }
        return false;
    }
    // validating name
    private boolean isValidName(String name) {
        if (name != null && name.length() > 2) {
            return true;
        }
        return false;
    }

    @Override
    public void onClick(View v) {

        if(v.getId() == R.id.btn_sign){
            //registration function
        registration();
            Log.d("regis",sFname+" "+sLname+" "+sEmail+" "+sPassword);
            //redirecting login page
            Intent intent = new Intent(Registration.this,login.class);
            startActivity(intent);
            finish();

        }

    }
}
