package com.example.lenovo.projectalpha;

import android.content.Intent;
import android.os.AsyncTask;
import android.support.annotation.NonNull;
import android.util.Log;
import android.widget.Toast;


import java.util.List;

public class dbinit {


    private static final String TAG = dbinit.class.getName();

    public static void populateAsync(@NonNull final AppDatabase db) {
        PopulateDbAsync task = new PopulateDbAsync(db);
        task.execute();
    }

    public static void populateSync(@NonNull final AppDatabase db) {
        populateWithTestData(db);
    }

    private static Product addUser(final AppDatabase db, Product product) {

        db.productDao().insertAll(product);
        return product;

    }

    private static void populateWithTestData(AppDatabase db) {

        String[] ItemName = new String[] { "Buger", "Pizza",
                "Potato fry", "Coffee","Drinks" };
        String[] ItemImg = new String[] { "burger", "pizza_slice","fried_potatoes","coffee","soda" };
        int[] pc = new int[] {10,20,25,32,22};
        int[] unit = new int[] {100,50,55,110,200};

        String name = null,pimg = null;
        int price = 0,quan = 0;

        for (int i=0; i<7; i++){
            name = ItemName[i];
            pimg = ItemImg[i];
            price = pc[i];
            quan = unit[i];

            Product p = new Product();
            p.setPname(name);
            p.setPrice(price);
            p.setQuantity(quan);
            p.setPimg(pimg);

            addUser(db,p);
        }









        // Toast.makeText( ,"Populating " + String.valueOf(userList.get(1)), Toast.LENGTH_SHORT ).show();
    }

    private static class PopulateDbAsync extends AsyncTask<Void, Void, Void> {

        private final AppDatabase mDb;

        PopulateDbAsync(AppDatabase db) {
            mDb = db;

        }

        @Override
        protected Void doInBackground(final Void... params) {
            populateWithTestData(mDb);
            return null;
        }

    }


}