package com.example.lenovo.projectalpha.Admin;

import android.content.Intent;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;
import android.widget.Toast;

import com.example.lenovo.projectalpha.AppDatabase;
import com.example.lenovo.projectalpha.ContentList;
import com.example.lenovo.projectalpha.ProductView;
import com.example.lenovo.projectalpha.R;

public class DeleteProduct extends AppCompatActivity implements View.OnClickListener{

    private EditText etDelName;
    private Button btnDel;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.delete_product);

        etDelName = findViewById(R.id.et_delete_name);
        btnDel = findViewById(R.id.btn_delete);

        btnDel.setOnClickListener(this);

    }

    @Override
    public void onClick(View view) {

        if(view.getId() == R.id.btn_delete){
            String name = etDelName.getText().toString();
            AppDatabase dbs = AppDatabase.getAppDatabase(this);
            int pid = dbs.productDao().productId(name);

            dbs.productDao().deleteProduct(pid);

            Toast.makeText(this,"Delete complete",Toast.LENGTH_SHORT).show();
            Intent intent = new Intent(DeleteProduct.this,AdminMenu.class);
            startActivity(intent);
            finish();
        }
    }
}
