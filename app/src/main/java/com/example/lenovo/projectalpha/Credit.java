package com.example.lenovo.projectalpha;

import android.os.Bundle;
import android.app.Activity;
import android.widget.EditText;
import android.widget.TextView;

public class Credit extends Activity {

    private String cName,cInfo,cDetail;
    private TextView tvCredit,tvInfo;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.credit);

        tvCredit =findViewById(R.id.tv_credit);
        tvInfo=findViewById(R.id.tv_info);

        cDetail= "This is a sample Coffee shop application. \n" +
                "This app content populated from Database. \n" +
                "Admin's both username and Password = admin \n" +
                "Buy Food item from Menu list.\n" +
                "" +
                "From Admin Panel: \n"+
                "* Create New Product \n"+
                "* Update Old Product \n"+
                "* Delete Old Product \n"
        ;

        cInfo = "Author. \n" +
                "        MD. Mohaiminul Hasan \n" +
                "        East West University \n" +
                "        ID: 2015-2-60-084 \n" +
                "Project for Mobile Game & Application training [ICT Division] ";


        tvCredit.setText(cInfo);
        tvInfo.setText(cDetail);
    }

}
