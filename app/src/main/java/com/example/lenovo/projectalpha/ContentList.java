package com.example.lenovo.projectalpha;

import android.content.Intent;
import android.content.SharedPreferences;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.view.View;
import android.widget.AdapterView;
import android.widget.ArrayAdapter;
import android.widget.EditText;
import android.widget.ListView;
import android.widget.TextView;
import android.widget.Toast;

import java.util.ArrayList;
import java.util.List;

public class ContentList extends AppCompatActivity implements AdapterView.OnItemClickListener{
    TextView tvTitle,tvDescription ;
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.content_list);

//        String[] values = new String[] { "Buger", "Donut", "Pizza",
//                 "Potato fry", "Coffee","Coke","Drinks" };
//        String[] values3 = new String[] { "burger", "donut","pizza_slice","fried_potatoes","coffee","cola","soda" };

        ListView listView = findViewById(R.id.lv1);

        final ArrayList<String> titleList = new ArrayList<String>();
        final ArrayList<String> imgList = new ArrayList<String>();

        //get product from db
        AppDatabase dbs = AppDatabase.getAppDatabase(this);
        List<Product> listProduct = dbs.productDao().getAllProduct();

        for(int i=0; i<listProduct.size(); i++){
            titleList.add(listProduct.get(i).getPname());
            imgList.add(listProduct.get(i).getPimg());
        }



//        listAdapter adapter = new listAdapter(this,titleList, desList);
//        listView.setAdapter(adapter);

        listView.setAdapter(new listAdapter(this,titleList,imgList));
        listView.setOnItemClickListener(this);
    }

    @Override
    public void onItemClick(AdapterView<?> adapterView, View view, int i, long l) {
        String item = (String) adapterView.getItemAtPosition(i).toString();
//        Toast.makeText(this, item, Toast.LENGTH_SHORT).show();


        SharedPreferences.Editor editor = getSharedPreferences("productsinfo",MODE_PRIVATE).edit();
        editor.putInt("currentPid",Integer.valueOf(item));
        editor.commit();

        Intent intent = new Intent(ContentList.this,ProductView.class);
        startActivity(intent);
        finish();
    }
}
