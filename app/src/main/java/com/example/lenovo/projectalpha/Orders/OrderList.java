package com.example.lenovo.projectalpha.Orders;

import android.content.SharedPreferences;
import android.support.annotation.IntegerRes;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.util.Log;
import android.widget.ListView;

import com.example.lenovo.projectalpha.AppDatabase;
import com.example.lenovo.projectalpha.Order;
import com.example.lenovo.projectalpha.OrderList_adapter;
import com.example.lenovo.projectalpha.R;
import com.example.lenovo.projectalpha.listAdapter;

import java.util.ArrayList;
import java.util.List;

public class OrderList extends AppCompatActivity {

    ListView lvOrder;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_order_list);

        lvOrder =findViewById(R.id.lv_order);

        final ArrayList<String> itemName = new ArrayList<String>();
        final ArrayList<Integer> itemRate= new ArrayList<Integer>();
        final ArrayList<Integer> itemQuantity = new ArrayList<Integer>();

        SharedPreferences p = getSharedPreferences("login", MODE_PRIVATE);
        String orderTimes = p.getString("orderTime","");
        Log.d("orderTIme",orderTimes);

        AppDatabase dbs = AppDatabase.getAppDatabase(this);
        List<Order> orders = dbs.orderDao().getOrderByTime(orderTimes);

        Log.d("orders", String.valueOf(orders.size()));

        for(int i=0; i<orders.size(); i++){
            itemName.add(orders.get(i).getOrderItemName());
            itemRate.add(orders.get(i).getOrderItemRate());
            itemQuantity.add(orders.get(i).getOrderItemQuantity());

        }

        lvOrder.setAdapter(new OrderList_adapter(this,itemName,itemRate,itemQuantity));

    }
}
