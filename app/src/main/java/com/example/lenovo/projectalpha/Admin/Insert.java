package com.example.lenovo.projectalpha.Admin;

import android.content.Intent;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.util.Log;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;
import android.widget.TextView;
import android.widget.Toast;

import com.example.lenovo.projectalpha.AppDatabase;
import com.example.lenovo.projectalpha.Contents;
import com.example.lenovo.projectalpha.Product;
import com.example.lenovo.projectalpha.R;

import java.util.List;

public class Insert extends AppCompatActivity implements View.OnClickListener {

    private EditText etPname,etPrice,etQuantity,etImage;
    private TextView etNote;
    private Button btnSubmit;
    private String sPname,sImage,cName;
    private int sPrice,sQuantity;




    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.insert_product);

        etPname = findViewById(R.id.et_pname);
        etPrice = findViewById(R.id.et_price);
        etQuantity = findViewById(R.id.et_quantity);
        etImage = findViewById(R.id.et_img);
        etNote = findViewById(R.id.tv_note);

        btnSubmit = findViewById(R.id.btn_insert);
        btnSubmit.setOnClickListener(this);

        cName = "Use one of this for image_name for product image. \n" +
                "chicken,   chinese,    burger,     coffee,  cola,   donut,  french_fries,   fried_egg,  fried_potatoes,    hotdog, \n" +
                "ice_cream,     ice_cream1,  milkshake\n";

        etNote.setText(cName);

    }

    @Override
    public void onClick(View view) {
        if(view.getId()== R.id.btn_insert){
            sPname= etPname.getText().toString();
            sPrice = Integer.valueOf(etPrice.getText().toString());
            sQuantity = Integer.valueOf(etQuantity.getText().toString());
            sImage= etImage.getText().toString();


        Product product = new Product();
        product.setPname(sPname);
        product.setQuantity(sQuantity);
        product.setPrice(sPrice);
        product.setPimg(sImage);

        AppDatabase dbs = AppDatabase.getAppDatabase(this);
        dbs.productDao().insertAll(product);

            List<Product> listProduct = dbs.productDao().getAllProduct();
            for (int i=0; i<listProduct.size(); i++){
                Log.d("product", String.valueOf(listProduct.get(i).getPname())+" id "+String.valueOf(listProduct.get(i).getPid()) );

            }
            Toast.makeText(this,sPname+" Product upload!",Toast.LENGTH_SHORT).show();
            Intent intent = new Intent(Insert.this,Contents.class);
            startActivity(intent);
            finish();
        }
    }
}
