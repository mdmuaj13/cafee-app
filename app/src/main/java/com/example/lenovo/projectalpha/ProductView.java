package com.example.lenovo.projectalpha;

import android.content.Intent;
import android.content.SharedPreferences;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.util.Log;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;
import android.widget.ImageView;
import android.widget.TextView;
import android.widget.Toast;

import com.google.gson.Gson;
import com.google.gson.reflect.TypeToken;

import java.lang.reflect.Type;
import java.text.DateFormat;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Date;
import java.util.List;

import static com.example.lenovo.projectalpha.R.drawable.coffee;
import static com.example.lenovo.projectalpha.R.drawable.cola;

public class ProductView extends AppCompatActivity implements View.OnClickListener {

    private EditText etAmount;
    private TextView tvTitle,tvQuantity,tvPrice;
    private Button btnBuy;
    private ImageView imgThumb;

    private String sTitle,sImage,sPrice,sQuantity;
    private int sAmount,sStock,sPid;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.product_view);

    tvTitle = findViewById(R.id.tv_product_title);
    tvQuantity = findViewById(R.id.tv_product_quantity);
    tvPrice = findViewById(R.id.tv_product_price);
    imgThumb =findViewById(R.id.iv_thumb);
    etAmount =findViewById(R.id.et_amount);

    btnBuy = findViewById(R.id.btn_buy);
    btnBuy.setOnClickListener(this);

        SharedPreferences  productData = getSharedPreferences("productsinfo",MODE_PRIVATE);
        int pid = productData.getInt("currentPid",1);
        pid++;
        sPid = pid;
        //get product from db
        final AppDatabase dbs = AppDatabase.getAppDatabase(this);
        Product onlyProduct = dbs.productDao().getProductById(pid);

        sTitle = onlyProduct.getPname().toString();
        sImage = onlyProduct.getPimg().toString();
        sPrice = String.valueOf(onlyProduct.getPrice());
        sQuantity = String.valueOf(onlyProduct.getQuantity());
        sStock=onlyProduct.getQuantity();

        Log.d("check",sTitle);
        Log.d("check",sImage);
        Log.d("check", String.valueOf(sPrice));
        Log.d("check", String.valueOf(sQuantity));

        tvTitle.setText(sTitle);
        tvPrice.setText(String.valueOf(sPrice)+ "  ৳");
        tvQuantity.setText(String.valueOf(sQuantity)+" Pc");
        int id = getResources().getIdentifier("drawable/"+sImage,null,getPackageName());
        imgThumb.setImageResource(id);
    }

    @Override
    public void onClick(View view) {
        if(view.getId() == R.id.btn_buy){
            try {
                sAmount = Integer.parseInt(String.valueOf(etAmount.getText()));
                Log.d("amount", String.valueOf(sAmount));


                if (sAmount <= sStock) {
                    int newAmount = sStock - sAmount;
                    Log.d("Newamount", String.valueOf(newAmount));
                    AppDatabase dbs = AppDatabase.getAppDatabase(this);
                    dbs.productDao().updateQuantity(newAmount, sPid);

                    int newQuantity = dbs.productDao().productQuantity(sTitle);
                    Log.d("Newamount", String.valueOf(newQuantity));
                    Toast.makeText(this, String.valueOf(sAmount) + " " + sTitle + " added on your order list", Toast.LENGTH_SHORT).show();


                    //get user info
                    SharedPreferences p = getSharedPreferences("login", MODE_PRIVATE);
                    int uid = p.getInt("uid", 1);
                    String ot = p.getString("orderTime","");
                    //Get Current time & date
                    SimpleDateFormat sdfDate = new SimpleDateFormat("yyyy-MM-dd HH:mm:ss");//dd/MM/yyyy
                    Date now = new Date();
                    String s = sdfDate.format(now);

                    //making order
                    Order order = new Order();
                    order.setOrder_time(s);
                    order.setUid(uid);
                    order.setOrderItemName(sTitle);
                    order.setPid(sPid);
                    order.setOrderItemRate(Integer.parseInt(sPrice));
                    order.setOrderItemQuantity(sAmount);

                    Log.d("ord", String.valueOf(order.getUid()));
                    Log.d("ord", String.valueOf(order.getOrderItemName()));
                    Log.d("ord", String.valueOf(order.getOrderItemRate()));
                    Log.d("ord", String.valueOf(order.getOrderItemQuantity()));
                    Log.d("ord", String.valueOf(order.getPid()));

                    dbs.orderDao().insertOrder(order);

//                    ArrayList<Order> orderList = new ArrayList<Order>();
//                    SharedPreferences.Editor ed = (SharedPreferences.Editor) getSharedPreferences("order",MODE_PRIVATE).edit();
//
//                    Gson gson = new Gson();
//                    String json = gson.toJson(order);
//
//                    ed.putString("ordersList",json);
//                    ed.apply();
//
//
//                    SharedPreferences got = getSharedPreferences("order",MODE_PRIVATE);
////                    Gson gSon = new Gson();
////                    got.getString("order","");
////                    Type type = new TypeToken<ArrayList<Order>>(){}.getType();
////                    ArrayList<Order> ord =gSon.fromJson(got, new TypeToken<List<Order>>(){}.getType());
//
//                    Gson gsonn = new Gson();
//                    String response=got.getString("order", "");
//                    ArrayList<Order> getList = gsonn.fromJson(response,
//                            new TypeToken<List<Order>>(){}.getType());
//
//                    for (int k=0; k<getList.size(); k++){
//                        Log.d("gson", String.valueOf(getList.get(k).getPid()));
//                        Log.d("gson", String.valueOf(getList.get(k).getiAmount()));
//                        Log.d("gson", String.valueOf(getList.get(k).getiRate()));
//                        Log.d("gson", String.valueOf(getList.get(k).getOrder_time()));
//                        Log.d("gson", String.valueOf(getList.get(k).getpName()));
//                    }



                    Intent intent = new Intent(ProductView.this, ContentList.class);
                    startActivity(intent);
                    finish();
                }
            }
            catch (Exception e){

            }
        }
    }
}
