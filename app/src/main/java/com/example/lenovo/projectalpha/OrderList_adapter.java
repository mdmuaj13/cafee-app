package com.example.lenovo.projectalpha;

import android.annotation.SuppressLint;
import android.content.Context;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.BaseAdapter;
import android.widget.ImageView;
import android.widget.TextView;

import com.example.lenovo.projectalpha.Orders.OrderList;

import java.util.ArrayList;

public class OrderList_adapter extends BaseAdapter {

    ArrayList<String> itemName;
    ArrayList<Integer> itemPrice;
    ArrayList<Integer> itemQuantity;
    Context context;
    private static LayoutInflater inflater = null;

    public OrderList_adapter(OrderList mainAcitivity, ArrayList<String> getName, ArrayList<Integer> getRate, ArrayList<Integer> getQuantity){
        itemName = getName;
        itemPrice = getRate;
        itemQuantity = getQuantity;
        context = mainAcitivity;

        Log.d("orders", String.valueOf(itemName));
        Log.d("orders", String.valueOf(itemPrice));
        Log.d("orders", String.valueOf(itemQuantity));

        inflater = (LayoutInflater) context.getSystemService(context.LAYOUT_INFLATER_SERVICE);

    }

    @Override
    public int getCount() {    return 0; }

    @Override
    public Object getItem(int i) {        return null;    }

    @Override
    public long getItemId(int i) { return 0;    }

    public class OrderHolder {
        TextView tvIname,tvIprice,tvIquantity,tvItotal;
    }

    @Override
    public View getView(int i, View viewOrder, ViewGroup viewGroup) {
         OrderHolder orderHolder = new OrderHolder();

        viewOrder = inflater.inflate(R.layout.order_list_adapter,null);

        orderHolder.tvIname = (TextView) viewOrder.findViewById(R.id.tv_item_name);
        orderHolder.tvIprice= (TextView) viewOrder.findViewById(R.id.tv_item_rate);
        orderHolder.tvIquantity = (TextView) viewOrder.findViewById(R.id.tv_item_quantity);
        orderHolder.tvItotal = (TextView) viewOrder.findViewById(R.id.tv_item_total);

        int total = itemPrice.get(i) * itemQuantity.get(i);

        orderHolder.tvIname.setText(itemName.get(i));
        orderHolder.tvIprice.setText(itemPrice.get(i));
        orderHolder.tvIquantity.setText(itemQuantity.get(i));
        orderHolder.tvItotal.setText(total);


        return viewOrder;

    }
}
