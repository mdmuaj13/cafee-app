package com.example.lenovo.projectalpha;

import android.arch.persistence.db.SupportSQLiteDatabase;
import android.arch.persistence.room.Database;
import android.arch.persistence.room.Room;
import android.arch.persistence.room.RoomDatabase;
import android.arch.persistence.room.migration.Migration;
import android.content.Context;

@Database(entities = {User.class,Product.class,Order.class}, version = 9)
public abstract class AppDatabase extends RoomDatabase {

    private static AppDatabase INSTANCE;

    public abstract UserDao userDao();
    public abstract ProductDao productDao();
    public abstract OrderDao orderDao();

    public static AppDatabase getAppDatabase(Context context){
        if(INSTANCE == null){
            INSTANCE = Room.databaseBuilder(context.getApplicationContext(),AppDatabase.class, "user_product_order")
                    .allowMainThreadQueries()
                    .addMigrations(MIGRATION_7_9)
                    .build();
        }
        return INSTANCE;
    }
    static final Migration MIGRATION_7_9= new Migration(7, 9) {
        @Override
        public void migrate(
                SupportSQLiteDatabase database) {
            // Since we didn’t alter the table, there’s nothing else
            // to do here.
        }
    };

    public static void destroyInstance(){
        INSTANCE = null;
    }

}
